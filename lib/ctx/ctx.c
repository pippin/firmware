#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>

#define CTX_MIN_EDGE_LIST_SIZE 256
#define CTX_MAX_EDGE_LIST_SIZE 512
#define CTX_MIN_JOURNAL_SIZE   512
#define CTX_MAX_JOURNAL_SIZE   512 // Doesn't really get used - since
                                   // we at the moment render synchronously
				   // from the data provided by core1
#define CTX_LIMIT_FORMATS       1
#define CTX_DITHER              1
#define CTX_ENABLE_RGB565_BYTESWAPPED 1
#define CTX_BITPACK_PACKER      0
#define CTX_COMPOSITING_GROUPS  0
#define CTX_RENDERSTREAM_STATIC 0
#define CTX_GRADIENT_CACHE      1
#define CTX_ENABLE_CLIP         1
#define CTX_BLOATY_FAST_PATHS   0
#define CTX_1BIT_CLIP           1
#define CTX_RASTERIZER_AA       3
#define CTX_RASTERIZER_FORCE_AA 1
#define CTX_SHAPE_CACHE         0
#define CTX_SHAPE_CACHE_DIM     16*18
#define CTX_SHAPE_CACHE_ENTRIES 128
#define CTX_RASTERIZER_MAX_CIRCLE_SEGMENTS 36
#define CTX_RASTERIZER          1
#define CTX_EVENTS              0
#define CTX_STRINGPOOL_SIZE     512
#define CTX_ENABLE_SHADOW_BLUR  0
#define CTX_FORMATTER           0
#define CTX_PARSER              0
#define CTX_FONTS_FROM_FILE     0
#define CTX_IMPLEMENTATION
#define _CTX_INTERNAL_FONT_   // defining this causes the internal font to be dropped
#include "ctx-font-regular.h" // font-regular contains the same outlines but more
                              // glyphs
#include "ctx.h"


