#ifndef CTX_FONT_regular
/* this is a ctx encoded font based on DejaVuSans.ttf */
/* CTX_SUBDIV:4  CTX_BAKE_FONT_SIZE:160 */
/* glyphs covered: 

 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghi
  jklmnopqrstuvwxyz{|}~éñÑßæøåö£ÆÖØÅ€§π°üÜ…”“«»©®™⏎⌫⌨·←↑↓→☣☀☁☂☢☭☮☯☽✉⚙⚠␣²◆♥♦♣
  ♠▲▼÷≈±≥≤½¼╴−╶╵╷┛┓┣╋┳┻┫┗┏┃━⎢⎥  */
static const struct __attribute__ ((packed)) {uint8_t code; uint32_t a; uint32_t b;}
ctx_font_regular[]={
{'@', 0x00000020, 0x00002bb0},/*                 x-advance: 43.687500 */
{'@', 0x00000021, 0x00003719},/*        !        x-advance: 55.097656 */
{'@', 0x00000022, 0x00003f38},/*        "        x-advance: 63.218750 */
{'@', 0x00000023, 0x0000732a},/*        #        x-advance: 115.164062 */
{'@', 0x00000024, 0x00005773},/*        $        x-advance: 87.449219 */
{'@', 0x00000025, 0x0000829a},/*        %        x-advance: 130.601562 */
{'@', 0x00000026, 0x00006b2e},/*        &        x-advance: 107.179688 */
{'@', 0x00000027, 0x000025c9},/*        '        x-advance: 37.785156 */
{'@', 0x00000028, 0x0000359f},/*        (        x-advance: 53.621094 */
{'@', 0x00000029, 0x0000359f},/*        )        x-advance: 53.621094 */
{'@', 0x0000002a, 0x000044b9},/*        *        x-advance: 68.722656 */
{'@', 0x0000002b, 0x0000732a},/*        +        x-advance: 115.164062 */
{'@', 0x0000002c, 0x00002bb0},/*        ,        x-advance: 43.687500 */
{'@', 0x0000002d, 0x00003198},/*        -        x-advance: 49.593750 */
{'@', 0x0000002e, 0x00002bb0},/*        .        x-advance: 43.687500 */
{'@', 0x0000002f, 0x00002e4f},/*        /        x-advance: 46.308594 */
{'@', 0x00000030, 0x00005773},/*        0        x-advance: 87.449219 */
{'@', 0x00000031, 0x00005773},/*        1        x-advance: 87.449219 */
{'@', 0x00000032, 0x00005773},/*        2        x-advance: 87.449219 */
{'@', 0x00000033, 0x00005773},/*        3        x-advance: 87.449219 */
{'@', 0x00000034, 0x00005773},/*        4        x-advance: 87.449219 */
{'@', 0x00000035, 0x00005773},/*        5        x-advance: 87.449219 */
{'@', 0x00000036, 0x00005773},/*        6        x-advance: 87.449219 */
{'@', 0x00000037, 0x00005773},/*        7        x-advance: 87.449219 */
{'@', 0x00000038, 0x00005773},/*        8        x-advance: 87.449219 */
{'@', 0x00000039, 0x00005773},/*        9        x-advance: 87.449219 */
{'@', 0x0000003a, 0x00002e4f},/*        :        x-advance: 46.308594 */
{'@', 0x0000003b, 0x00002e4f},/*        ;        x-advance: 46.308594 */
{'@', 0x0000003c, 0x0000732a},/*        <        x-advance: 115.164062 */
{'@', 0x0000003d, 0x0000732a},/*        =        x-advance: 115.164062 */
{'@', 0x0000003e, 0x0000732a},/*        >        x-advance: 115.164062 */
{'@', 0x0000003f, 0x000048f3},/*        ?        x-advance: 72.949219 */
{'@', 0x00000040, 0x00008973},/*        @        x-advance: 137.449219 */
{'@', 0x00000041, 0x00005e06},/*        A        x-advance: 94.023438 */
{'@', 0x00000042, 0x00005e4b},/*        B        x-advance: 94.292969 */
{'@', 0x00000043, 0x00005ff9},/*        C        x-advance: 95.972656 */
{'@', 0x00000044, 0x000069d6},/*        D        x-advance: 105.835938 */
{'@', 0x00000045, 0x000056d8},/*        E        x-advance: 86.843750 */
{'@', 0x00000046, 0x00004f0f},/*        F        x-advance: 79.058594 */
{'@', 0x00000047, 0x00006a82},/*        G        x-advance: 106.507812 */
{'@', 0x00000048, 0x0000675b},/*        H        x-advance: 103.355469 */
{'@', 0x00000049, 0x00002889},/*        I        x-advance: 40.535156 */
{'@', 0x0000004a, 0x00002889},/*        J        x-advance: 40.535156 */
{'@', 0x0000004b, 0x00005a22},/*        K        x-advance: 90.132812 */
{'@', 0x0000004c, 0x00004c93},/*        L        x-advance: 76.574219 */
{'@', 0x0000004d, 0x00007697},/*        M        x-advance: 118.589844 */
{'@', 0x0000004e, 0x000066d1},/*        N        x-advance: 102.816406 */
{'@', 0x0000004f, 0x00006c30},/*        O        x-advance: 108.187500 */
{'@', 0x00000050, 0x000052e2},/*        P        x-advance: 82.882812 */
{'@', 0x00000051, 0x00006c30},/*        Q        x-advance: 108.187500 */
{'@', 0x00000052, 0x00005f80},/*        R        x-advance: 95.500000 */
{'@', 0x00000053, 0x0000573f},/*        S        x-advance: 87.246094 */
{'@', 0x00000054, 0x000053f5},/*        T        x-advance: 83.957031 */
{'@', 0x00000055, 0x0000649a},/*        U        x-advance: 100.601562 */
{'@', 0x00000056, 0x00005e06},/*        V        x-advance: 94.023438 */
{'@', 0x00000057, 0x000087e7},/*        W        x-advance: 135.902344 */
{'@', 0x00000058, 0x00005e29},/*        X        x-advance: 94.160156 */
{'@', 0x00000059, 0x000053f5},/*        Y        x-advance: 83.957031 */
{'@', 0x0000005a, 0x00005e29},/*        Z        x-advance: 94.160156 */
{'@', 0x0000005b, 0x0000359f},/*        [        x-advance: 53.621094 */
{'@', 0x0000005c, 0x00002e4f},/*       \         x-advance: 46.308594 */
{'@', 0x0000005d, 0x0000359f},/*        ]        x-advance: 53.621094 */
{'@', 0x0000005e, 0x0000732a},/*        ^        x-advance: 115.164062 */
{'@', 0x0000005f, 0x000044b9},/*        _        x-advance: 68.722656 */
{'@', 0x00000060, 0x000044b9},/*        `        x-advance: 68.722656 */
{'@', 0x00000061, 0x0000543a},/*        a        x-advance: 84.226562 */
{'@', 0x00000062, 0x0000573f},/*        b        x-advance: 87.246094 */
{'@', 0x00000063, 0x00004b92},/*        c        x-advance: 75.570312 */
{'@', 0x00000064, 0x0000573f},/*        d        x-advance: 87.246094 */
{'@', 0x00000065, 0x00005490},/*        e        x-advance: 84.562500 */
{'@', 0x00000066, 0x00003063},/*        f        x-advance: 48.386719 */
{'@', 0x00000067, 0x0000573f},/*        g        x-advance: 87.246094 */
{'@', 0x00000068, 0x0000571d},/*        h        x-advance: 87.113281 */
{'@', 0x00000069, 0x00002630},/*        i        x-advance: 38.187500 */
{'@', 0x0000006a, 0x00002630},/*        j        x-advance: 38.187500 */
{'@', 0x0000006b, 0x00004f98},/*        k        x-advance: 79.593750 */
{'@', 0x0000006c, 0x00002630},/*        l        x-advance: 38.187500 */
{'@', 0x0000006d, 0x000085e4},/*        m        x-advance: 133.890625 */
{'@', 0x0000006e, 0x0000571d},/*        n        x-advance: 87.113281 */
{'@', 0x0000006f, 0x00005418},/*        o        x-advance: 84.093750 */
{'@', 0x00000070, 0x0000573f},/*        p        x-advance: 87.246094 */
{'@', 0x00000071, 0x0000573f},/*        q        x-advance: 87.246094 */
{'@', 0x00000072, 0x00003882},/*        r        x-advance: 56.507812 */
{'@', 0x00000073, 0x0000479c},/*        s        x-advance: 71.609375 */
{'@', 0x00000074, 0x000035e4},/*        t        x-advance: 53.890625 */
{'@', 0x00000075, 0x0000571d},/*        u        x-advance: 87.113281 */
{'@', 0x00000076, 0x00005157},/*        v        x-advance: 81.339844 */
{'@', 0x00000077, 0x0000706a},/*        w        x-advance: 112.414062 */
{'@', 0x00000078, 0x00005157},/*        x        x-advance: 81.339844 */
{'@', 0x00000079, 0x00005157},/*        y        x-advance: 81.339844 */
{'@', 0x0000007a, 0x00004825},/*        z        x-advance: 72.144531 */
{'@', 0x0000007b, 0x00005773},/*        {        x-advance: 87.449219 */
{'@', 0x0000007c, 0x00002e4f},/*        |        x-advance: 46.308594 */
{'@', 0x0000007d, 0x00005773},/*        }        x-advance: 87.449219 */
{'@', 0x0000007e, 0x0000732a},/*        ~        x-advance: 115.164062 */
{'@', 0x000000e9, 0x00005490},/*        é        x-advance: 84.562500 */
{'@', 0x000000f1, 0x0000571d},/*        ñ        x-advance: 87.113281 */
{'@', 0x000000d1, 0x000066d1},/*        Ñ        x-advance: 102.816406 */
{'@', 0x000000df, 0x00005693},/*        ß        x-advance: 86.574219 */
{'@', 0x000000e6, 0x000086f7},/*        æ        x-advance: 134.964844 */
{'@', 0x000000f8, 0x00005418},/*        ø        x-advance: 84.093750 */
{'@', 0x000000e5, 0x0000543a},/*        å        x-advance: 84.226562 */
{'@', 0x000000f6, 0x00005418},/*        ö        x-advance: 84.093750 */
{'@', 0x000000a3, 0x00005773},/*        £        x-advance: 87.449219 */
{'@', 0x000000c6, 0x000085e4},/*        Æ        x-advance: 133.890625 */
{'@', 0x000000d6, 0x00006c30},/*        Ö        x-advance: 108.187500 */
{'@', 0x000000d8, 0x00006c30},/*        Ø        x-advance: 108.187500 */
{'@', 0x000000c5, 0x00005e06},/*        Å        x-advance: 94.023438 */
{'@', 0x000020ac, 0x00005773},/*        €        x-advance: 87.449219 */
{'@', 0x000000a7, 0x000044b9},/*        §        x-advance: 68.722656 */
{'@', 0x000003c0, 0x000052c0},/*        π        x-advance: 82.750000 */
{'@', 0x000000b0, 0x000044b9},/*        °        x-advance: 68.722656 */
{'@', 0x000000fc, 0x0000571d},/*        ü        x-advance: 87.113281 */
{'@', 0x000000dc, 0x0000649a},/*        Ü        x-advance: 100.601562 */
{'@', 0x00002026, 0x00008973},/*        …        x-advance: 137.449219 */
{'@', 0x0000201d, 0x00004735},/*        ”        x-advance: 71.207031 */
{'@', 0x0000201c, 0x00004735},/*        “        x-advance: 71.207031 */
{'@', 0x000000ab, 0x00005418},/*        «        x-advance: 84.093750 */
{'@', 0x000000bb, 0x00005418},/*        »        x-advance: 84.093750 */
{'@', 0x000000a9, 0x00008973},/*        ©        x-advance: 137.449219 */
{'@', 0x000000ae, 0x00008973},/*        ®        x-advance: 137.449219 */
{'@', 0x00002122, 0x00008973},/*        ™        x-advance: 137.449219 */
{'@', 0x000023ce, 0x0000732a},/*        ⏎        x-advance: 115.164062 */
{'@', 0x0000232b, 0x0000c25c},/*        ⌫        x-advance: 194.359375 */
{'@', 0x00002328, 0x0000c663},/*        ⌨        x-advance: 198.386719 */
{'@', 0x000000b7, 0x00002bb0},/*        ·        x-advance: 43.687500 */
{'@', 0x00002190, 0x0000732a},/*        ←        x-advance: 115.164062 */
{'@', 0x00002191, 0x0000732a},/*        ↑        x-advance: 115.164062 */
{'@', 0x00002193, 0x0000732a},/*        ↓        x-advance: 115.164062 */
{'@', 0x00002192, 0x0000732a},/*        →        x-advance: 115.164062 */
{'@', 0x00002623, 0x00007b38},/*        ☣        x-advance: 123.218750 */
{'@', 0x00002600, 0x00007b38},/*        ☀        x-advance: 123.218750 */
{'@', 0x00002601, 0x00008973},/*        ☁        x-advance: 137.449219 */
{'@', 0x00002602, 0x00007b38},/*        ☂        x-advance: 123.218750 */
{'@', 0x00002622, 0x00007b38},/*        ☢        x-advance: 123.218750 */
{'@', 0x0000262d, 0x00007b38},/*        ☭        x-advance: 123.218750 */
{'@', 0x0000262e, 0x00007b38},/*        ☮        x-advance: 123.218750 */
{'@', 0x0000262f, 0x00007b38},/*        ☯        x-advance: 123.218750 */
{'@', 0x0000263d, 0x00007b38},/*        ☽        x-advance: 123.218750 */
{'@', 0x00002709, 0x0000732a},/*        ✉        x-advance: 115.164062 */
{'@', 0x00002699, 0x00007b38},/*        ⚙        x-advance: 123.218750 */
{'@', 0x000026a0, 0x00007b38},/*        ⚠        x-advance: 123.218750 */
{'@', 0x00002423, 0x0000573f},/*        ␣        x-advance: 87.246094 */
{'@', 0x000000b2, 0x00003719},/*        ²        x-advance: 55.097656 */
{'@', 0x000025c6, 0x000069b4},/*        ◆        x-advance: 105.703125 */
{'@', 0x00002665, 0x00007b38},/*        ♥        x-advance: 123.218750 */
{'@', 0x00002666, 0x00007b38},/*        ♦        x-advance: 123.218750 */
{'@', 0x00002663, 0x00007b38},/*        ♣        x-advance: 123.218750 */
{'@', 0x00002660, 0x00007b38},/*        ♠        x-advance: 123.218750 */
{'@', 0x000025b2, 0x000069b4},/*        ▲        x-advance: 105.703125 */
{'@', 0x000025bc, 0x000069b4},/*        ▼        x-advance: 105.703125 */
{'@', 0x000000f7, 0x0000732a},/*        ÷        x-advance: 115.164062 */
{'@', 0x00002248, 0x0000732a},/*        ≈        x-advance: 115.164062 */
{'@', 0x000000b1, 0x0000732a},/*        ±        x-advance: 115.164062 */
{'@', 0x00002265, 0x0000732a},/*        ≥        x-advance: 115.164062 */
{'@', 0x00002264, 0x0000732a},/*        ≤        x-advance: 115.164062 */
{'@', 0x000000bd, 0x00008538},/*        ½        x-advance: 133.218750 */
{'@', 0x000000bc, 0x00008538},/*        ¼        x-advance: 133.218750 */
{'@', 0x00002574, 0x000052c0},/*        ╴        x-advance: 82.750000 */
{'@', 0x00002212, 0x0000732a},/*        −        x-advance: 115.164062 */
{'@', 0x00002576, 0x000052c0},/*        ╶        x-advance: 82.750000 */
{'@', 0x00002575, 0x000052c0},/*        ╵        x-advance: 82.750000 */
{'@', 0x00002577, 0x000052c0},/*        ╷        x-advance: 82.750000 */
{'@', 0x0000251b, 0x000052c0},/*        ┛        x-advance: 82.750000 */
{'@', 0x00002513, 0x000052c0},/*        ┓        x-advance: 82.750000 */
{'@', 0x00002523, 0x000052c0},/*        ┣        x-advance: 82.750000 */
{'@', 0x0000254b, 0x000052c0},/*        ╋        x-advance: 82.750000 */
{'@', 0x00002533, 0x000052c0},/*        ┳        x-advance: 82.750000 */
{'@', 0x0000253b, 0x000052c0},/*        ┻        x-advance: 82.750000 */
{'@', 0x0000252b, 0x000052c0},/*        ┫        x-advance: 82.750000 */
{'@', 0x00002517, 0x000052c0},/*        ┗        x-advance: 82.750000 */
{'@', 0x0000250f, 0x000052c0},/*        ┏        x-advance: 82.750000 */
{'@', 0x00002503, 0x000052c0},/*        ┃        x-advance: 82.750000 */
{'@', 0x00002501, 0x000052c0},/*        ━        x-advance: 82.750000 */
{'@', 0x000023a2, 0x000044b9},/*        ⎢        x-advance: 68.722656 */
{'@', 0x000023a5, 0x000044b9},/*        ⎥        x-advance: 68.722656 */
};
#define CTX_FONT_regular 1
#endif
