var group__PWRSEQ__Register__Offsets =
[
    [ "MXC_R_PWRSEQ_LPCN", "group__PWRSEQ__Register__Offsets.html#gaf8c2b11606fbb27db53a94550588c114", null ],
    [ "MXC_R_PWRSEQ_LPWKST0", "group__PWRSEQ__Register__Offsets.html#ga983fffe86f6bf2507240984507b0eedf", null ],
    [ "MXC_R_PWRSEQ_LPWKEN0", "group__PWRSEQ__Register__Offsets.html#ga9d02050422bbde2399a294d16e379ecd", null ],
    [ "MXC_R_PWRSEQ_LPWKST1", "group__PWRSEQ__Register__Offsets.html#ga6fd3ae151d4daab6523e20e240182b94", null ],
    [ "MXC_R_PWRSEQ_LPWKEN1", "group__PWRSEQ__Register__Offsets.html#gaeab920c7ff9518b03f53fe72ef503782", null ],
    [ "MXC_R_PWRSEQ_LPPWST", "group__PWRSEQ__Register__Offsets.html#ga261f3bc5075aa2b742ad94e2b2a74528", null ],
    [ "MXC_R_PWRSEQ_LPPWEN", "group__PWRSEQ__Register__Offsets.html#gaaf7833587e750d94ef8caa2f7608e1c6", null ],
    [ "MXC_R_PWRSEQ_LPMEMSD", "group__PWRSEQ__Register__Offsets.html#gab9037c3f57d36953a518da17d47f29f6", null ],
    [ "MXC_R_PWRSEQ_LPVDDPD", "group__PWRSEQ__Register__Offsets.html#gae3553e68300164d9e059aa72f72d7643", null ],
    [ "MXC_R_PWRSEQ_BURETVEC", "group__PWRSEQ__Register__Offsets.html#ga0cba11b6c61c42fb64b29e1a56fb9d6c", null ],
    [ "MXC_R_PWRSEQ_BUAOD", "group__PWRSEQ__Register__Offsets.html#gacadc8aa1a940e5c882afc80f05114eb3", null ]
];