#pragma once

#include <stdint.h>
#include "wsf_types.h"

#include "att_api.h"
#include "dm_api.h"

#define CARD10_UUID_SUFFIX                                                     \
	0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23, 0x42, 0x23
#define CARD10_UUID_PREFIX 0x02, 0x23, 0x42

/**************************************************************************************************
  Function Declarations
**************************************************************************************************/
/*************************************************************************************************/
/*!
 *  \brief  Start the application.
 *
 *  \return None.
 */
/*************************************************************************************************/
void BleStart(void);


/* ATT client module interface. Used by main BLE module */
void bleValueUpdate(attEvt_t *pMsg);
void bleDiscCback(dmConnId_t connId, uint8_t status);
