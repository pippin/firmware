/*
 * Copyright 2019-2020 Øyvind Kolås <pippin@gimp.org>
 */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#if CARD10_CTX_STATIC /* stand-alone, we've got more headroom than
                         the core0 firmware and will include a larger
                         font*/

#ifndef CTX_SIMULATOR
#define CTX_MIN_JOURNAL_SIZE          32 // we do not need a journal,
#define CTX_MAX_JOURNAL_SIZE          32 // we render direct
#define CTX_EVENTS                     0
#endif

#define CTX_DITHER                     1
#define CTX_RENDERSTREAM_STATIC        0

#define CTX_RASTERIZER_AA       3
#define CTX_RASTERIZER_FORCE_AA 1
#define CTX_BITPACK_PACKER      0
#define CTX_GRADIENT_CACHE      1
#define CTX_1BIT_CLIP           1

#define CTX_RASTERIZER_MAX_CIRCLE_SEGMENTS 36

#define CTX_LIMIT_FORMATS              1
#define CTX_ENABLE_GRAY1               1
#define CTX_ENABLE_GRAY1               1
#define CTX_ENABLE_GRAY8               1
#define CTX_ENABLE_RGB565              1
#define CTX_ENABLE_RGB565_BYTESWAPPED  1
#define CTX_RASTERIZER                 1
#define CTX_COMPOSITE                  1
#define CTX_FONTS_FROM_FILE            0
#define CTX_IMPLEMENTATION
#define _CTX_INTERNAL_FONT_ // dropping internal and using:
#include "ctx-font-regular.h"
#include "ctx.h"

#else
#define CTX_RASTERIZER                 0
#define CTX_COMPOSITE                  0
#define CTX_BITPACK_PACKER             0
#define CTX_SHAPE_CACHE                0
#define CTX_GRADIENT_CACHE             0
#define CTX_MIN_JOURNAL_SIZE        2048
#define CTX_MAX_JOURNAL_SIZE        2048
#define CTX_RENDERSTREAM_STATIC        0
#define CTX_EVENTS                     0
#define CTX_FORMATTER                  0
#define CTX_PARSER                     1
#define CTX_FONTS_FROM_FILE            0
#define CTX_IMPLEMENTATION             1
#define _CTX_INTERNAL_FONT_ // dropping internal and using:
//#include "ctx-font-ascii-spacing.h"
#include "ctx-font-regular-spacing.h"
#include "ctx.h"
#endif


#include "ui.h"

/* XXX globals */
int frame_no = 0;
#if CTX_SIMULATOR
int ui_long_press_delay   = 140;
#else
int ui_long_press_delay   = 300;
#endif
int ui_press_repeat_delay = 400;
unsigned int ui_scene_timer = 0;
int   ui_no = 0;     /* the item in the UI to run fullscreen,
                                 if it is a menu, that menu is rendered -
                                 but this can also be used for other
                                 UI control, where the menus are only
                                 one of the states
                                 */

float ui_font_size   = 18.0f;
float ui_line_height = 1.2f;

int   ui_selected_no = 0; /* the item with focus */
int   ui_item_active = 0; /* slider being edited */

float ui_scroll_jump = 0.66;

float ui_y = 0.0;
float ui_x = 0.0;

int ui_start_menu = 0;
#define MAX_MENU_DEPTH 8
int ui_menu_stack_start[MAX_MENU_DEPTH];
int ui_menu_stack_selected[MAX_MENU_DEPTH];
int ui_menu_depth = 0;

static UiItem  *ui_items = NULL;

void ui_set_items (UiItem *items)
{
  ui_items = items;
}

float ui_item_draw_fullscreen (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_DRAW_FULLSCREEN);
}

float ui_item_height (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_HEIGHT);
}

static float ui_item_draw (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_DRAW);
}

static float ui_item_activate  (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_ACTIVATE);
}

static float ui_item_active_events (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_ACTIVE_EVENTS);
}

static float ui_item_draw_background (Ctx *ctx, UiItem *item)
{
  return item->handler(ctx, item, ITEM_DRAW_BACKGROUND);
}

float ui_label (Ctx *ctx, UiItem *item, UiItemAction action)
{
  int selected = (item == &ui_items[ui_selected_no])?1:0;

  switch (action)
  {
    case ITEM_DRAW_FULLSCREEN:
      break;
    case ITEM_DRAW_BACKGROUND:
      if (selected)
      {
        ctx_begin_path (ctx);
        ctx_round_rectangle (ctx, 0, ui_y - ui_font_size, 
                     DISP_WIDTH, ui_item_height (ctx, item), ui_font_size*0.25);
        ctx_gray (ctx, 1.0f);
        ctx_fill (ctx);
        ctx_rgba8 (ctx, 0,0,0,255);
      }
      else
      {
        ctx_rgba8 (ctx, 255,255,255,255);
      }
      ctx_move_to   (ctx, ui_x, ui_y);
      break;
    case ITEM_DRAW:
      ctx_text (ctx, item->title);
      break;
    case ITEM_HEIGHT:
      return ui_font_size * ui_line_height;
    case ITEM_ACTIVATE:
      break;
    case ITEM_ACTIVE_EVENTS:
      break;
  }
  return 0.0f;
}

float ui_separator (Ctx *ctx, UiItem *item, UiItemAction action) {
  switch (action)
  {
    case ITEM_DRAW:
      ctx_gray (ctx, 0.5);
      ctx_line_width (ctx, -1.0);
      ctx_move_to (ctx, 0.0, ui_y - ui_font_size * 0.7);
      ctx_rel_line_to (ctx, DISP_WIDTH, 0.0);
      ctx_stroke (ctx);
      return 0.0f;
    case ITEM_HEIGHT:
      return ui_font_size * 0.3;
    default:
      return ui_label (ctx, item, action);
  }
}

float ui_button (Ctx *ctx, UiItem *item, UiItemAction action) {
  switch (action)
  {
    case ITEM_ACTIVATE:
      {
        void (*cb)(Ctx *ctx, void *data) = item->value;
        cb (ctx, item->data);
        return 0.0;
      }
    default:
      return ui_label (ctx, item, action);
  }
}

float ui_boolean_toggle (Ctx *ctx, UiItem *item, UiItemAction action) {
  switch (action)
  {
    case ITEM_DRAW: 
      ctx_text (ctx, item->title);
      ctx_text (ctx, ((int*)(item->value))[0]?" on":" off");
      return 0.0f;
    case ITEM_ACTIVATE:
      {
        int *val = (int*)item->value;
        *val = !(*val);
        return 0.0f;
      }
    default:
      return ui_label (ctx, item, action);
  }
}

float ui_float_slider (Ctx *ctx, UiItem *item, UiItemAction action)
{
  int selected = (item == &ui_items[ui_selected_no])?1:0;
  float handle_width = ui_font_size/2;
  float min  = item->arg1;
  float max  = item->arg2;
  float step = item->arg3;

  switch (action)
  {
    case ITEM_DRAW:
      {

      char buf[64];
      float val = ((float*)(item->value))[0];
      float rel_val = (val - min) / (max - min);
      ctx_round_rectangle (ctx, rel_val * (DISP_WIDTH - handle_width),
                           ui_y - ui_font_size, 
                     handle_width, ui_font_size * ui_line_height, ui_font_size*0.25);
      if (ui_item_active && selected)
        ctx_rgb (ctx, 1.0, 0.4, 0.2f);
      else
        ctx_rgb (ctx, 0.3, 0.4, 1.0f);
      ctx_fill (ctx);
    if (selected)
      ctx_rgba8 (ctx, 0,0,0,255);
    else
      ctx_rgba8 (ctx, 255,255,255,255);
    ctx_move_to   (ctx, ui_x, ui_y);

      ctx_text (ctx, item->title);
      ctx_text (ctx, " ");
      if (val < 0)
      {
        ctx_text (ctx, "-");
        val = -val;
      }
      int integer = val;
      float decimals = (val - integer) * 100;
      sprintf (buf, "%i.", integer);
      ctx_text (ctx, buf);
      if      (decimals < 10)   ctx_text (ctx, "0");

      sprintf (buf, "%i", (int)(decimals));
      ctx_text (ctx, buf);
      }
      return 0.0f;
    case ITEM_ACTIVATE:
         ui_item_active = 1;
        return 0.0f;
    case ITEM_ACTIVE_EVENTS:

      if (ui_event_match (BUTTON_RIGHT_TOP, UI_PRESS))
      {
         ui_item_active = 0;
      }
      if (ui_event_match (BUTTON_RIGHT_BOTTOM, UI_PRESS_REPEAT))
      {
         float val = ((float*)item->value)[0];

         val += step;
         if (val > max)
            val = max;

         ((float*)item->value)[0] = val;
      }
      if (ui_event_match (BUTTON_LEFT_BOTTOM, UI_PRESS_REPEAT) && frame_no > 2)
      {
         float val = ((float*)item->value)[0];

         val -= step;
         if (val < min)
            val = min;

         ((float*)item->value)[0] = val;
      }
      return 0.0f;

    default:
      return ui_label (ctx, item, action);
  }
}

float ui_int_slider (Ctx *ctx, UiItem *item, UiItemAction action)
{
  int selected = (item == &ui_items[ui_selected_no])?1:0;
  float handle_width = ui_font_size/2;
  float min  = item->arg1;
  float max  = item->arg2;
  float step = item->arg3;

  switch (action)
  {
    case ITEM_DRAW:
      {
      float val = ((int*)(item->value))[0];
      float rel_val = (val - min) / (max - min);
      ctx_round_rectangle (ctx, rel_val * (DISP_WIDTH - handle_width),
                           ui_y - ui_font_size, 
                     handle_width, ui_font_size * ui_line_height, ui_font_size*0.25);
      if (ui_item_active && selected)
        ctx_rgb (ctx, 8.0, 0.6, 0.2f);
      else
        ctx_rgb (ctx, 0.3, 0.4, 1.0f);

      ctx_fill (ctx);
      if (selected)
        ctx_rgba8 (ctx, 0,0,0,255);
      else
        ctx_rgba8 (ctx, 255,255,255,255);
      ctx_move_to   (ctx, ui_x, ui_y);

      char buf[64];
      ctx_text (ctx, item->title);
      sprintf (buf, " %i", ((int*)(item->value))[0]);
      ctx_text (ctx, buf);
            }
         return 0.0f;

    case ITEM_ACTIVATE:
         ui_item_active = 1;
         return 0.0f;
    case ITEM_ACTIVE_EVENTS:

    if (ui_event_match (BUTTON_RIGHT_TOP, UI_PRESS)){
       ui_item_active = 0;
    }
    if (ui_event_match (BUTTON_RIGHT_BOTTOM, UI_PRESS_REPEAT)){
       float val = ((int*)item->value)[0];

       val += step;
       if (val > max)
          val = max;

       ((int*)item->value)[0] = val;
    }
    if (ui_event_match (BUTTON_LEFT_BOTTOM, UI_PRESS_REPEAT) && frame_no > 2)
    {
       float val = ((int*)item->value)[0];

       val -= step;
       if (val < min)
          val = min;

       ((int*)item->value)[0] = val;
    }
    return 0.0f;

    default:
      return ui_label (ctx, item, action);
  }
}

int ui_find_menu (const char *name);
static void  ui_menu_pos_push (void);
static void  ui_menu_pos_pop (void);

static void ui_menu_pos_push (void)
{
  if (ui_menu_depth >= MAX_MENU_DEPTH) return;
  ui_menu_stack_start[ui_menu_depth]=ui_start_menu;
  ui_menu_stack_selected[ui_menu_depth]=ui_selected_no;
  if (ui_menu_depth+1 < MAX_MENU_DEPTH)
    ui_menu_depth++;
}

float ui_sub_menu (Ctx *ctx, UiItem *item, UiItemAction action)
{
  switch (action)
  {
    case ITEM_DRAW:
      ctx_text (ctx, item->title);
      ctx_text (ctx, "/");
      return 0.0f;
    case ITEM_ACTIVATE:
      ui_menu_pos_push ();
      ui_start_menu = ui_find_menu (item->title);
      ui_selected_no = ui_start_menu + 1;
      return 0.0f;
    default:
      return ui_label (ctx, item, action);
  }
}

int ui_autoplay = 0;
int ui_ui_autoplay_timeout = 1000 * 1;
int ui_title_timeout    = 1000 * 2;
unsigned int ui_ticks  = 0;

static void draw_title (Ctx *ctx)
{
  if ((int)(ui_ticks - ui_scene_timer) > ui_title_timeout) return;
  ctx_rgba (ctx, 0.0f, 0.0f, 0.0f, 0.5f);
  ctx_move_to (ctx, -1, 80-4.0+1);
  ctx_font_size (ctx, ui_font_size);
  ctx_text (ctx, ui_items[ui_no].title);
  ctx_rgb (ctx, 1.0f, 1.0f, 0.0f);
  ctx_move_to (ctx, 0, 80-4.0);
  ctx_text (ctx, ui_items[ui_no].title);
}


UiItem *ui_current_scene (void)
{
  return &ui_items[ui_no];
}


static void ui_menu_pos_pop (void)
{
  if (ui_menu_depth > 0)
  {
    ui_start_menu = ui_menu_stack_start[ui_menu_depth-1];
    ui_selected_no = ui_menu_stack_selected[ui_menu_depth-1];
    ui_menu_depth--;
    ui_reset_timer ();
    ui_no = ui_start_menu;
  }
}


float ui_menu (Ctx *ctx, UiItem *item, UiItemAction action)
{
  if (action == ITEM_DRAW_FULLSCREEN)
  {
  static float scroll = 0.0;
  ui_x = 4.0;
  ui_y = ui_font_size;

  ctx_save (ctx);
  ctx_translate (ctx, 0.0, -scroll);
  ctx_font_size (ctx, ui_font_size);

  /* estimates */
  float desired_bottom_scroll = (ui_selected_no-ui_start_menu+2) * ui_font_size * ui_line_height - 80;
  float desired_top_scroll = (ui_selected_no-ui_start_menu - 1) * ui_font_size * ui_line_height;

  for (int i = ui_start_menu; ui_items[i].handler && (i == ui_start_menu || ui_items[i].handler!=ui_menu); i++)
  {
    float height = ui_item_height (ctx, &ui_items[i]);

    if (i == ui_selected_no)
    {
       desired_top_scroll = ui_y - ui_font_size * ui_line_height * 1.5;
       //desired_bottom_scroll = ui_y - 80.0 + ui_font_size * ui_line_height;
       desired_bottom_scroll = ui_y - ui_font_size * ui_line_height + height - 80.0 + ui_font_size * ui_line_height;
    }

  if (ui_y - scroll - ui_font_size > DISP_HEIGHT ||
      ui_y + height - scroll < 0)
  {
    /* out of view */
  }
  else
  {
    ui_item_draw_background (ctx, &ui_items[i]);
    ui_item_draw (ctx, &ui_items[i]);
  }

    ui_y += ui_item_height (ctx, &ui_items[i]);
  }
  ctx_restore (ctx);

  if (desired_bottom_scroll  > scroll)
  {
    scroll = desired_bottom_scroll * ui_scroll_jump + (1.0-ui_scroll_jump) * scroll;
  }
  if (desired_top_scroll  < scroll)
  {
    scroll = desired_top_scroll * ui_scroll_jump + (1.0-ui_scroll_jump) * scroll;
  }

  if (ui_item_active)
  {
    ui_item_active_events (ctx, &ui_items[ui_selected_no]);
  }
  else
  {
    if (ui_event_match (BUTTON_RIGHT_TOP, UI_PRESS))
    {
       ui_reset_timer ();
       ui_item_activate (ctx, &ui_items[ui_selected_no]);
    }
    if (ui_event_match (BUTTON_RIGHT_BOTTOM, UI_PRESS_REPEAT))
    {
       if (ui_items[ui_selected_no+1].title != NULL &&
           ui_items[ui_selected_no+1].handler != ui_menu)
         ui_selected_no ++;

       if (ui_items[ui_selected_no].handler == ui_separator)
         ui_selected_no ++;
    }
    if (ui_event_match (BUTTON_LEFT_BOTTOM, UI_PRESS_REPEAT) && frame_no > 2)
    {
       if (ui_items[ui_selected_no].handler == ui_menu)
       {
          ui_menu_pos_pop ();
          return 0.0f;
       }

       ui_selected_no--;
       if (ui_selected_no < 0) ui_selected_no = 0;

       if (ui_items[ui_selected_no].handler == ui_separator)
         ui_selected_no --;
       if (ui_selected_no < 0) ui_selected_no = 0;
       if (ui_selected_no < ui_start_menu)
       {
          ui_selected_no = ui_start_menu;
       }
    }
  }
    return 0.0f;
  }
  return ui_label (ctx, item, action);
}

int ui_find_menu (const char *name)
{
  for (int i = 0; ui_items[i].title; i++)
  {
    if (ui_items[i].handler == ui_menu && !strcmp (ui_items[i].title, name))
      return i;
  }
  return 0;
}

extern UiItem ctx_demo[];

float ui_scene (Ctx *ctx, UiItem *item, UiItemAction action);


void ui_advance (void)
{
  static int first_menu = 1;

  ui_reset_timer ();
  ui_no++;
  if (ui_items[ui_no].handler == ui_menu)
  {
    if (first_menu)
    {
      first_menu = 0;
      ui_start_menu = ui_no;
      ui_selected_no = ui_no + 1;
    }
    else
    {
      while (ui_items[ui_no-1].handler == ui_scene)
        ui_no --;
    }
  }
}

void ui_reverse (void)
{
  ui_reset_timer ();
  if (ui_no>0)
  {
    if (ui_items[ui_no-1].handler == ui_scene)
      ui_no--;
  }
}

float ui_scene (Ctx *ctx, UiItem *item, UiItemAction action)
{
  int timeout = item->arg1 * 1000;

  switch (action)
  {
   case ITEM_DRAW_FULLSCREEN:
   {
    float (*fun)(Ctx *ctx, int frame_no) = item->value;
    if (fun)
      fun (ctx, frame_no);
    draw_title (ctx);

    if ((frame_no > 3 && ui_event_match (BUTTON_RIGHT_BOTTOM, UI_PRESS)) ||
        (ui_autoplay && (int)(ui_ticks - ui_scene_timer) > ui_ui_autoplay_timeout) ||
        (timeout && (int)(ui_ticks - ui_scene_timer) > timeout)
        )
    {
      ui_advance ();
    }
    if (ui_event_match (BUTTON_LEFT_BOTTOM, UI_LONG_PRESS))
    {
       int old_ui_no = ui_no;
       ui_menu_pos_pop ();
       ui_selected_no = old_ui_no;
    }
    else if (ui_event_match (BUTTON_LEFT_BOTTOM, UI_PRESS))
    {
      ui_reverse ();
    }
    return 0.0f;
  }
   case ITEM_ACTIVATE:
     ui_menu_pos_push ();
     ui_no = ui_selected_no;
     ui_reset_timer ();
     return 0.0f;
   default:
    return ui_label (ctx, item, action);
  }
}

unsigned int ui_ticks_live (void)
{
  static uint64_t ui_time_start;
  static uint64_t ui_time_now = 0;
  if (ui_time_start == 0)
  {
#if CTX_SIMULATOR
    ui_time_start = ctx_ticks ()/1000;
#else
    ui_time_start = epic_rtc_get_milliseconds ();
#endif
  }
#if CTX_SIMULATOR
  ui_time_now = ctx_ticks ()/1000;
#else
  ui_time_now = epic_rtc_get_milliseconds ();
#endif
  ui_ticks = (ui_time_now - ui_time_start);
  return ui_ticks;
}

void ui_reset_timer (void)
{
  frame_no = 0;
  ui_scene_timer = ui_ticks;
}

/* event handling code, that gathers events and abstracts
 * them to long and short presses as well as pressed/not
 */

static int32_t state      = 0;
static int32_t prev_state = 0;

static int long_press_mask      = 0;
static int prev_long_press_mask = 0;
#if CTX_SIMULATOR

static int32_t sim_state = 0;

static int release_button (Ctx *ctx, void *data1)
{
  int button = (size_t)(data1);
  if (sim_state & button)
    sim_state = sim_state - button;
  return 0;
}

static void button_sim (CtxEvent *event, void *data1, void *data2)
{
  int button = (size_t)(data1);
  sim_state |= button;
  ctx_add_timeout (event->ctx, 20, release_button, data1);
}

#endif

int ui_event_update (Ctx *ctx)
{
#if CTX_SIMULATOR
  static int inited = 0;
  if (!inited)
  {
    inited = 1;
  }
  ctx_add_key_binding (ctx, "q", NULL, "",           button_sim, (void*)BUTTON_LEFT_TOP);
  ctx_add_key_binding (ctx, "esc", NULL, "",         button_sim, (void*)BUTTON_LEFT_TOP);
  ctx_add_key_binding (ctx, "left", NULL, "",        button_sim, (void*)BUTTON_LEFT_BOTTOM);
  ctx_add_key_binding (ctx, "right", NULL, "",       button_sim, (void*)BUTTON_RIGHT_BOTTOM);
  ctx_add_key_binding (ctx, "up", NULL, "",          button_sim, (void*)BUTTON_RIGHT_TOP);

  CtxEvent *event;
  usleep (500);
  while ((event = ctx_get_event (ctx)));
#endif

  int ret = 0;
  prev_state = state;
  prev_long_press_mask = long_press_mask;
  long_press_mask = 0;
  ui_ticks_live (); // updates the variable used directly in rest of code
#if CTX_SIMULATOR
  state = sim_state;
#else
  int32_t new_state = epic_buttons_read
      (BUTTON_LEFT_BOTTOM|BUTTON_RIGHT_BOTTOM|BUTTON_RIGHT_TOP|BUTTON_LEFT_TOP);
  state = new_state;
#endif
  return ret;
}

static inline int leftmost_bit_set (int number)
{
  int rshifts = 0;
  while (number != 0)
  {
    rshifts ++;
    number >>= 1;
  }
  return rshifts - 1;
}

static int press_time[4] = {0,};

int ui_event_match (int button, UiEventType event_type)
{
  int ret = 0;
  int button_no = leftmost_bit_set (button);

  switch (event_type)
  {
    case UI_PRESSED:
      ret = ((state & button) != 0);
      break;
    case UI_PRESS_REPEAT:
      {
        int is_pressed   = (state & button)  != 0;
        int got_pressed  = is_pressed && ((prev_state & button) == 0);

        if (got_pressed ||
            (is_pressed &&
             (ui_ticks > (unsigned)press_time[button_no] + ui_press_repeat_delay)))
        {
          press_time[button_no] = ui_ticks;
          ret = 1;
        }
      }
      break;
    case UI_PRESS:
    {
      int got_pressed = ((state & button)  != 0) &&
                        ((prev_state & button)  == 0);
      int got_released = ((state & button)  == 0) &&
                        ((prev_state & button)  != 0);

      if (prev_long_press_mask & button)
      {
         if (got_pressed)
         {
            press_time[button_no] = ui_ticks;
            ret = 0;
         }
         else
         {
            if (got_released)
            {
              if (ui_ticks < (unsigned)press_time[button_no] + ui_long_press_delay)
              {
                press_time[button_no] = 0;
                ret = 1;
              }
            }
         }
      } else
      {
         ret = got_pressed;
      }
    }
      break;
    case UI_RELEASE:
      ret = ((state & button)  == 0) &&
            ((prev_state & button)  != 0);
      if (ret)
      {
         press_time[button_no] = 0;
      }
      break;
    case UI_LONG_PRESS:
      long_press_mask |= button;
      if  (((state & button) != 0) &&
            ui_ticks > (unsigned)press_time[button_no] + ui_long_press_delay &&
            press_time[button_no]
)
      {
        ret = 1;
        press_time[button_no] = 0;
      }
      break;
  }
  return ret;
}
