#ifndef __UI_H_
#define __UI_H_
/*
 * Copyright 2019-2020 Øyvind Kolås <pippin@gimp.org>
 */

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#if CTX_SIMULATOR
#define DISP_WIDTH   160
#define DISP_HEIGHT  80

enum epic_button {
        /** ``1``, Bottom left button (bit 0). */
        BUTTON_LEFT_BOTTOM   = 1,
        /** ``2``, Bottom right button (bit 1). */
        BUTTON_RIGHT_BOTTOM  = 2,
        /** ``4``, Top right button (bit 2). */
        BUTTON_RIGHT_TOP     = 4,
        /** ``8``, Top left (power) button (bit 3). */
        BUTTON_LEFT_TOP      = 8,
        /** ``8``, Top left (power) button (bit 3). */
        BUTTON_RESET         = 8,
};


#else
#include "epicardium.h"
#endif

typedef enum  {
  ITEM_DRAW_BACKGROUND, /* draws the round rectangle highlighting selected item,
                            */
  ITEM_DRAW,            
  ITEM_HEIGHT,   /* returns vertical space occupied by item */
  ITEM_ACTIVATE, /* upper right from menu, can for a toggle button
                    or button activating trigger event
                  */
  ITEM_ACTIVE_EVENTS, /* handle events for when this item is selected and
                         activated  */
  ITEM_DRAW_FULLSCREEN /* draw this item full screen, makes most sense
                          for menu and other custom top-level handlers
                          */
} UiItemAction;

enum _UiEventType
{
  UI_PRESSED,
  UI_PRESS,
  UI_PRESS_REPEAT,
  UI_LONG_PRESS,
  UI_RELEASE,
};
typedef enum _UiEventType UiEventType;

int ui_event_update (Ctx *ctx);
int ui_event_match (int button, UiEventType type);

/* XXX globals */
extern int frame_no;
extern unsigned int ui_scene_timer;
extern int   ui_no;     /* the item in the UI to run fullscreen,
                                 if it is a menu, that menu is rendered -
                                 but this can also be used for other
                                 UI control, where the menus are only
                                 one of the states
                                 */

extern float ui_font_size;
extern float ui_line_height;
extern float ui_y;
extern float ui_x;

//extern int ui_start_menu;

typedef struct _UiItem UiItem;
struct _UiItem{
  const char *title;
  float (*handler)(Ctx *ctx, UiItem *item, UiItemAction action);
  void *value; /*   */
  void *data;
  float arg1;
  float arg2;
  float arg3;
};
void  ui_set_items            (UiItem *items);
float ui_item_draw_fullscreen (Ctx *ctx, UiItem *item);
float ui_item_height          (Ctx *ctx, UiItem *item);

float ui_menu           (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_label          (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_separator      (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_button         (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_boolean_toggle (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_float_slider   (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_int_slider     (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_sub_menu       (Ctx *ctx, UiItem *item, UiItemAction action);
float ui_scene          (Ctx *ctx, UiItem *item, UiItemAction action);

//unsigned int ui_ticks_live (void); 
extern unsigned int ui_ticks;

extern int   ui_autoplay;
extern int   ui_ui_autoplay_timeout;
extern int   ui_title_timeout;

extern float ui_scroll_jump;

UiItem *ui_current_scene (void);
void   ui_reset_timer (void);

//int   ui_find_menu      (const char *name);
void  ui_advance (void);
void  ui_reverse (void);

int ui_event_update ();
int ui_event_match (int button, UiEventType event_type);

#endif
