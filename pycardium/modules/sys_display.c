#include "epicardium.h"
#include <stdint.h>

#include "py/obj.h"
#include "py/objint.h"
#include "py/objstr.h"
#include "py/runtime.h"

//#include <stdio.h>

#if 1
#define CTX_MIN_JOURNAL_SIZE    384
#define CTX_MAX_JOURNAL_SIZE    384
#define CTX_RASTERIZER          0
#define CTX_GRADIENT_CACHE      0
#define CTX_LIMIT_FORMATS       1
#define CTX_RENDERSTREAM_STATIC 1
#define CTX_EVENTS              0
#define CTX_PARSER              0
#define CTX_FORMATTER           0
#define CTX_FONTS_FROM_FILE     0
#define CTX_IMPLEMENTATION      1
#define _CTX_INTERNAL_FONT_

#define malloc  m_malloc
#define realloc m_realloc
#define free    m_free
#include "../../lib/ctx/ctx-font-regular-spacing.h"
#include "../../lib/ctx/ctx.h"
#undef free
#undef malloc
#undef realloc

static Ctx *ctx = NULL;

void full_cb (CtxRenderstream *rs, void *data)
{
	epic_disp_ctx (rs->entries, rs->count * 9);
	rs->count = 0;
}

static Ctx *ensure_ctx (void)
{
  if (!ctx)
  {
    ctx = ctx_new ();
    ctx_reset (ctx);
    //ctx_set_full_cb (ctx, full_cb, NULL);


  }
  CtxRenderstream *rs = &ctx->renderstream;

  /* with full_cb this used to be more precise - and
   * it will go wrong with very long text strings.
   */
  if (rs->count > CTX_MAX_JOURNAL_SIZE-64)
  {
    epic_disp_ctx (rs->entries, rs->count * 9);
    rs->count = 0;
  }
  return ctx;
}
#endif

static uint16_t rgb888_to_rgb565(uint8_t *bytes)
{
	return ((bytes[0] & 0b11111000) << 8) | ((bytes[1] & 0b11111100) << 3) |
	       (bytes[2] >> 3);
}

static inline void rgb565_to_rgb888 (uint16_t pixel, 
                                     uint8_t *red,
                                     uint8_t *green, 
                                     uint8_t *blue) 
{ 
  *blue   = (pixel & 31)<<3; 
  *green  = ((pixel>>5) & 63)<<2; 
  *red    = ((pixel>>11) & 31)<<3; 
}

static uint16_t get_color(mp_obj_t color_in)
{
	if (mp_obj_get_int(mp_obj_len(color_in)) < 3) {
		mp_raise_ValueError("color must have 3 elements");
	}

	uint8_t red = mp_obj_get_int(
		mp_obj_subscr(color_in, mp_obj_new_int(0), MP_OBJ_SENTINEL)
	);
	uint8_t green = mp_obj_get_int(
		mp_obj_subscr(color_in, mp_obj_new_int(1), MP_OBJ_SENTINEL)
	);
	uint8_t blue = mp_obj_get_int(
		mp_obj_subscr(color_in, mp_obj_new_int(2), MP_OBJ_SENTINEL)
	);
	uint8_t colors[3] = { red, green, blue };
	return rgb888_to_rgb565(colors);
}

static int ctx_mode = 0;
static int ctx_mode_text = 2;
static int ctx_commands_received = 0;

/* print something on the display */
static mp_obj_t mp_display_print(size_t n_args, const mp_obj_t *args)
{
	if (!mp_obj_is_str_or_bytes(args[0])) {
		mp_raise_TypeError("input text must be a string");
	}
	GET_STR_DATA_LEN(args[0], print, print_len);
	int32_t posx = mp_obj_get_int(args[1]);
	int32_t posy = mp_obj_get_int(args[2]);
	uint32_t fg  = get_color(args[3]);
	uint32_t bg  = get_color(args[4]);
	int res = 0;
	if (ctx_mode || ctx_mode_text) {
		Ctx *ctx = ensure_ctx();
		float font_size = 20.0f;
		ctx_font_size (ctx, font_size);
		if (fg!=bg) {
	  		uint8_t r,g,b;
          		rgb565_to_rgb888 (bg, &r, &g, &b);
	  		ctx_rgba8 (ctx, r, g, b, 255);
	  		ctx_rectangle (ctx, posx, posy,
				       ctx_text_width (ctx, (const char*)print), font_size);
	  		ctx_fill (ctx);
		}
		{
	  		uint8_t r,g,b;
          		rgb565_to_rgb888 (fg, &r, &g, &b);
	  		ctx_rgba8 (ctx, r, g, b, 255);
		}
		ctx_move_to (ctx, posx, posy + font_size * 0.8f);
		ctx_text (ctx, (const char*)print);
		if (!ctx_mode)
		{
			CtxRenderstream *rs = &ctx->renderstream;
			epic_disp_ctx (rs->entries, rs->count * 9);
    		        ctx_reset (ctx);
                        ctx_commands_received=0;
			rs->count = 0;
		}
	} else {
		res = epic_disp_print(posx, posy, (const char *)print, fg, bg);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_print_obj, 5, 5, mp_display_print
);

/* print something on the display */
static mp_obj_t mp_display_print_adv(size_t n_args, const mp_obj_t *args)
{
	if (!mp_obj_is_str_or_bytes(args[0])) {
		mp_raise_TypeError("input text must be a string");
	}
	GET_STR_DATA_LEN(args[0], print, print_len);
	int32_t posx     = mp_obj_get_int(args[1]);
	int32_t posy     = mp_obj_get_int(args[2]);
	uint32_t fg      = get_color(args[3]);
	uint32_t bg      = get_color(args[4]);
	uint8_t fontName = mp_obj_get_int(args[5]);
	int res = 0;
	if (ctx_mode || ctx_mode_text) {
		Ctx *ctx = ensure_ctx();
		float font_size = 20.0f;
		switch (fontName)
		{
	  		case 0: font_size = 8.0;  break;
	  		case 1: font_size = 12.0; break;
	  		case 2: font_size = 16.0; break;
	  		case 3: font_size = 20.0; break;
	  		case 4: font_size = 24.0; break;
	  		case 5: font_size = 32.0; break;
	  		case 6: font_size = 40.0; break;
	  		case 7: font_size = 64.0; break;
	  		case 8: font_size = 80.0; break;
		}
		ctx_font_size (ctx, font_size);
		if (fg != bg) {
			uint8_t r,g,b;
			rgb565_to_rgb888 (bg, &r, &g, &b);
	  		ctx_rgba8 (ctx, r, g, b, 255);
	  		ctx_rectangle (ctx, posx, posy,
			               ctx_text_width (ctx, (const char*)print), font_size);
	  		ctx_fill (ctx);
		}
		{
			uint8_t r,g,b;
			rgb565_to_rgb888 (fg, &r, &g, &b);
			ctx_rgba8 (ctx, r, g, b, 255);
		}
		ctx_move_to (ctx, posx, posy + font_size * 0.8);
		ctx_text (ctx, (const char*)print);
		if (!ctx_mode)
		{
			CtxRenderstream *rs = &ctx->renderstream;
			epic_disp_ctx (rs->entries, rs->count * 9);
    		        ctx_reset (ctx);
                        ctx_commands_received=0;
			rs->count = 0;
		}
	} else {
		res = epic_disp_print_adv(
                	fontName, posx, posy, (const char *)print, fg, bg);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_print_adv_obj, 6, 6, mp_display_print_adv
);

/* draw pixel on the display */
static mp_obj_t mp_display_pixel(size_t n_args, const mp_obj_t *args)
{
	int16_t x    = mp_obj_get_int(args[0]);
	int16_t y    = mp_obj_get_int(args[1]);
	uint16_t col = get_color(args[2]);

	int res = 0;
	if (ctx_mode) {
		Ctx *ctx = ensure_ctx();
		uint8_t r,g,b;
		rgb565_to_rgb888 (col, &r, &g, &b);
#if 0
		ctx_rgb (ctx, r/255.0, g/255.0, b/255.0);
		ctx_rectangle (ctx, x, y, 1, 1);
		ctx_fill (ctx);
#else
		ctx_set_pixel_u8 (ctx, x, y, r, g, b, 255);
#endif
	} else {
		res = epic_disp_pixel (x, y, col);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_pixel_obj, 3, 3, mp_display_pixel
);

/* set display backlight brightness */
static mp_obj_t mp_display_backlight(size_t n_args, const mp_obj_t *args)
{
	uint16_t brightness = mp_obj_get_int(args[0]);

	int res = epic_disp_backlight(brightness);
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_backlight_obj, 1, 1, mp_display_backlight
);



/* draw line on the display */
static mp_obj_t mp_display_line(size_t n_args, const mp_obj_t *args)
{
	int16_t xs   = mp_obj_get_int(args[0]);
	int16_t ys   = mp_obj_get_int(args[1]);
	int16_t xe   = mp_obj_get_int(args[2]);
	int16_t ye   = mp_obj_get_int(args[3]);
	uint16_t col = get_color(args[4]);
	uint16_t ls  = mp_obj_get_int(args[5]);
	uint16_t ps  = mp_obj_get_int(args[6]);

	if (ls > 1) {
		mp_raise_ValueError("Line style has to be 0 or 1");
	}

	int res = 0;
	if (ctx_mode) {
		Ctx *ctx = ensure_ctx();
		ctx_line_width (ctx, ps);
		ctx_move_to (ctx, xs, ys);
		ctx_line_to (ctx, xe, ye);
		uint8_t r,g,b;
		rgb565_to_rgb888 (col, &r, &g, &b);
		if (ls == LINESTYLE_DOTTED)
			ctx_rgba8 (ctx, r, g, b, 127);
		else
			ctx_rgba8 (ctx, r, g, b, 255);
		ctx_stroke (ctx);
	} else {
		res = epic_disp_line(xs, ys, xe, ye, col, ls, ps);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_line_obj, 7, 7, mp_display_line
);

/* draw rectangle on the display */
static mp_obj_t mp_display_rect(size_t n_args, const mp_obj_t *args)
{
	int16_t xs   = mp_obj_get_int(args[0]);
	int16_t ys   = mp_obj_get_int(args[1]);
	int16_t xe   = mp_obj_get_int(args[2]);
	int16_t ye   = mp_obj_get_int(args[3]);
	uint16_t col = get_color(args[4]);
	uint16_t fs  = mp_obj_get_int(args[5]);
	uint16_t ps  = mp_obj_get_int(args[6]);

	if (fs > 1) {
		mp_raise_ValueError("Fill style has to be 0 or 1");
	}

	int res = 0;
	if (ctx_mode) {
		Ctx *ctx = ensure_ctx();
		ctx_rectangle (ctx, xs, ys, xe-xs, ye-ys);
		uint8_t r,g,b;
		rgb565_to_rgb888 (col, &r, &g, &b);
		ctx_rgba8 (ctx, r, g, b, 255);

		if (fs == FILLSTYLE_FILLED)
		{
			ctx_fill (ctx);
		}
		else
		{
			ctx_line_width (ctx, ps);
			ctx_stroke (ctx);
		}
	} else {
		res = epic_disp_rect(xs, ys, xe, ye, col, fs, ps);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_rect_obj, 7, 7, mp_display_rect
);

/* draw rectangle on the display */
static mp_obj_t mp_display_circ(size_t n_args, const mp_obj_t *args)
{
	int16_t x    = mp_obj_get_int(args[0]);
	int16_t y    = mp_obj_get_int(args[1]);
	uint16_t rad = mp_obj_get_int(args[2]);
	uint16_t col = get_color(args[3]);
	uint16_t fs  = mp_obj_get_int(args[4]);
	uint16_t ps  = mp_obj_get_int(args[5]);

	if (fs > 1) {
	  mp_raise_ValueError("Fill style has to be 0 or 1");
	}

	int res = 0;
	if (ctx_mode) {
		Ctx *ctx = ensure_ctx();
		ctx_arc (ctx, x, y, rad, 0.0, CTX_PI*1.85, 0);
		uint8_t r,g,b;
		rgb565_to_rgb888 (col, &r, &g, &b);
		if (fs == FILLSTYLE_EMPTY && ps == LINESTYLE_DOTTED)
			ctx_rgba8 (ctx, r, g, b, 127);
		/*XXX:dotted and half transparent isn't really the same thing.. */
		else
			ctx_rgba8 (ctx, r, g, b, 255);

		if (fs == FILLSTYLE_FILLED) {
			ctx_fill (ctx);
		} else {
			ctx_line_width (ctx, ps);
			ctx_stroke (ctx);
		}
	} else {
		res = epic_disp_circ(x, y, rad, col, fs, ps);
	}
	if (res < 0) {
	  mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_circ_obj, 6, 6, mp_display_circ
);

/* clear the display */
static mp_obj_t mp_display_clear(mp_obj_t col)
{
	uint16_t color = get_color(col);
	int res = 0;
	if (ctx_mode) {
		Ctx *ctx = ensure_ctx();
        	ctx_reset (ctx);
		uint8_t r,g,b;
		rgb565_to_rgb888 (color, &r, &g, &b);
		ctx_rgba8 (ctx, r, g, b, 255);
		ctx_rectangle (ctx, 0, 0, DISP_WIDTH, DISP_HEIGHT);
		ctx_fill (ctx);
    	} else {
		res = epic_disp_clear(color);
	}
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_clear_obj, mp_display_clear);

static mp_obj_t mp_display_update()
{
	if (ctx_mode || ctx_commands_received) {
		Ctx *ctx = ensure_ctx ();
    		epic_disp_ctx (ctx->renderstream.entries,
			       ctx->renderstream.count * 9);
    		ctx_reset (ctx);
                ctx_commands_received=0;
	}
	int res = epic_disp_update();
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_update_obj, mp_display_update);

static mp_obj_t mp_display_open()
{
	int res = epic_disp_open();
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_open_obj, mp_display_open);

static mp_obj_t mp_display_close()
{
	int res = epic_disp_close();
	if (res < 0) {
		mp_raise_OSError(-res);
	}
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_close_obj, mp_display_close);

static mp_obj_t mp_display_scale(mp_obj_t arg0, mp_obj_t arg1)
{
	float x  = mp_obj_get_float(arg0);
	float y  = mp_obj_get_float(arg1);
        ctx_scale (ensure_ctx(), x, y);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_scale_obj, mp_display_scale);

static mp_obj_t mp_display_translate(mp_obj_t arg0, mp_obj_t arg1)
{
	float x  = mp_obj_get_float(arg0);
	float y  = mp_obj_get_float(arg1);
        ctx_translate (ensure_ctx(), x, y);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_translate_obj, mp_display_translate);

static mp_obj_t mp_display_rotate(mp_obj_t arg)
{
	float x1  = mp_obj_get_float(arg);
        ctx_rotate (ensure_ctx(), x1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_rotate_obj, mp_display_rotate);

static mp_obj_t mp_display_line_width(mp_obj_t arg)
{
	float x1  = mp_obj_get_float(arg);
        ctx_line_width (ensure_ctx(), x1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_line_width_obj,
                                 mp_display_line_width);

static mp_obj_t mp_display_gray(mp_obj_t arg)
{
	float x1  = mp_obj_get_float(arg);
        ctx_gray (ensure_ctx(), x1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_gray_obj,
                                 mp_display_gray);

static mp_obj_t mp_display_global_alpha(mp_obj_t arg)
{
	float x1  = mp_obj_get_float(arg);
        ctx_global_alpha (ensure_ctx(), x1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_global_alpha_obj,
                                 mp_display_global_alpha);

static mp_obj_t mp_display_font_size(mp_obj_t arg)
{
	float x1  = mp_obj_get_float(arg);
        ctx_font_size (ensure_ctx(), x1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_font_size_obj,
                                 mp_display_font_size);



static mp_obj_t mp_display_line_to(mp_obj_t arg0, mp_obj_t arg1)
{
	float x1  = mp_obj_get_float(arg0);
	float y1  = mp_obj_get_float(arg1);
        ctx_line_to (ensure_ctx(), x1, y1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_line_to_obj,
                                 mp_display_line_to);

static mp_obj_t mp_display_move_to(mp_obj_t arg0, mp_obj_t arg1)
{
	float x1 = mp_obj_get_float(arg0);
	float y1 = mp_obj_get_float(arg1);
        ctx_move_to (ensure_ctx(), x1, y1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_move_to_obj,
                                 mp_display_move_to);

static mp_obj_t mp_display_quad_to(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
        ctx_quad_to (ensure_ctx(), x1, y1, x2, y2);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_quad_to_obj, 4, 4, mp_display_quad_to
);

static mp_obj_t mp_display_linear_gradient(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
        ctx_linear_gradient (ensure_ctx(), x1, y1, x2, y2);
	return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_linear_gradient_obj, 4, 4, mp_display_linear_gradient
);

static mp_obj_t mp_display_radial_gradient(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float r1  = mp_obj_get_float(args[2]);
	float x2  = mp_obj_get_float(args[3]);
	float y2  = mp_obj_get_float(args[4]);
	float r2  = mp_obj_get_float(args[5]);
        ctx_radial_gradient (ensure_ctx(), x1, y1, r1, x2, y2, r2);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_radial_gradient_obj, 6, 6, mp_display_radial_gradient
);

static mp_obj_t mp_display_rectangle(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
        ctx_rectangle (ensure_ctx(), x1, y1, x2, y2);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_rectangle_obj, 4, 4, mp_display_rectangle
);


static mp_obj_t mp_display_curve_to(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
	float x3  = mp_obj_get_float(args[4]);
	float y3  = mp_obj_get_float(args[5]);
        ctx_curve_to (ensure_ctx(), x1, y1, x2, y2, x3, y3);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_curve_to_obj, 6, 6, mp_display_curve_to
);

static mp_obj_t mp_display_arc(size_t n_args, const mp_obj_t *args)
{
	float x  = mp_obj_get_float(args[0]);
	float y  = mp_obj_get_float(args[1]);
	float radius  = mp_obj_get_float(args[2]);
	float angle1  = mp_obj_get_float(args[3]);
	float angle2  = mp_obj_get_float(args[4]);
	int direction  = mp_obj_get_int(args[5]);
        ctx_arc (ensure_ctx(), x, y, radius, angle1, angle2, direction);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_arc_obj, 6, 6, mp_display_arc
);

static mp_obj_t mp_display_rel_line_to(mp_obj_t arg0, mp_obj_t arg1)
{
	float x1  = mp_obj_get_float(arg0);
	float y1  = mp_obj_get_float(arg1);
        ctx_rel_line_to (ensure_ctx(), x1, y1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_rel_line_to_obj,
                                 mp_display_rel_line_to);

static mp_obj_t mp_display_rel_move_to(mp_obj_t arg0, mp_obj_t arg1)
{
	float x1  = mp_obj_get_float(arg0);
	float y1  = mp_obj_get_float(arg1);
        ctx_rel_move_to (ensure_ctx(), x1, y1);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_2(display_rel_move_to_obj,
                                 mp_display_rel_move_to);

static mp_obj_t mp_display_rel_quad_to(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
        ctx_rel_quad_to (ensure_ctx(), x1, y1, x2, y2);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_rel_quad_to_obj, 4, 4, mp_display_rel_quad_to
);

static mp_obj_t mp_display_rel_curve_to(size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
	float x3  = mp_obj_get_float(args[4]);
	float y3  = mp_obj_get_float(args[5]);
        ctx_rel_curve_to (ensure_ctx(), x1, y1, x2, y2, x3, y3);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_rel_curve_to_obj, 6, 6, mp_display_rel_curve_to
);

static mp_obj_t mp_display_rgba (size_t n_args, const mp_obj_t *args)
{
	float x1  = mp_obj_get_float(args[0]);
	float y1  = mp_obj_get_float(args[1]);
	float x2  = mp_obj_get_float(args[2]);
	float y2  = mp_obj_get_float(args[3]);
        ctx_rgba (ensure_ctx(), x1, y1, x2, y2);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_rgba_obj, 4, 4, mp_display_rgba
);

static mp_obj_t mp_display_gradient_add_stop (size_t n_args, const mp_obj_t *args)
{
	float pos  = mp_obj_get_float(args[0]);
	float r  = mp_obj_get_float(args[1]);
	float g  = mp_obj_get_float(args[2]);
	float b  = mp_obj_get_float(args[3]);
	float a  = mp_obj_get_float(args[4]);
        ctx_gradient_add_stop (ensure_ctx(), pos, r, g, b, a);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(
	display_gradient_add_stop_obj, 5, 5, mp_display_gradient_add_stop
);

static mp_obj_t mp_display_fill ()
{
        ctx_fill (ensure_ctx());
        ctx_commands_received ++;
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_fill_obj, mp_display_fill);

static mp_obj_t mp_display_clip ()
{
        ctx_clip (ensure_ctx());
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_clip_obj, mp_display_clip);

static mp_obj_t mp_display_stroke ()
{
        ctx_stroke (ensure_ctx());
        ctx_commands_received ++;
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_stroke_obj, mp_display_stroke);

static mp_obj_t mp_display_save ()
{
        ctx_save (ensure_ctx());
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_save_obj, mp_display_save);

static mp_obj_t mp_display_restore ()
{
        ctx_restore (ensure_ctx());
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_restore_obj,
                                 mp_display_restore);

static mp_obj_t mp_display_begin_path ()
{
        ctx_begin_path (ensure_ctx());
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_begin_path_obj, mp_display_begin_path);

static mp_obj_t mp_display_close_path ()
{
        ctx_close_path (ensure_ctx());
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_0(display_close_path_obj,
                                 mp_display_close_path);

/* print something on the display */
static mp_obj_t mp_display_text(mp_obj_t arg)
{
	if (!mp_obj_is_str_or_bytes(arg)) {
		mp_raise_TypeError("input text must be a string");
	}
	GET_STR_DATA_LEN(arg, print, print_len);
	ctx_text (ensure_ctx(), (const char *)print);
        ctx_commands_received ++;
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_text_obj, mp_display_text);

static mp_obj_t mp_display_font(mp_obj_t arg)
{
	if (!mp_obj_is_str_or_bytes(arg)) {
		mp_raise_TypeError("arg must be string");
	}
	GET_STR_DATA_LEN(arg, print, print_len);
	ctx_font (ensure_ctx(), (const char *)print);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_font_obj, mp_display_font);

#if 0
static mp_obj_t mp_svg_path(mp_obj_t arg)
{
	if (!mp_obj_is_str_or_bytes(arg)) {
		mp_raise_TypeError("argument must be a string");
	}
	GET_STR_DATA_LEN(arg, d, print_len);
	ctx_svg_path (ensure_ctx(), (const char *)d);
	return mp_const_none;
}
static MP_DEFINE_CONST_FUN_OBJ_1(display_svg_path_obj, mp_svg_path);
#endif



/* The globals table for this module */
static const mp_rom_map_elem_t display_module_globals_table[] = {
	{ MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_sys_display) },
	{ MP_ROM_QSTR(MP_QSTR_open), MP_ROM_PTR(&display_open_obj) },
	{ MP_ROM_QSTR(MP_QSTR_close), MP_ROM_PTR(&display_close_obj) },
	{ MP_ROM_QSTR(MP_QSTR_print), MP_ROM_PTR(&display_print_obj) },
	{ MP_ROM_QSTR(MP_QSTR_print_adv), MP_ROM_PTR(&display_print_adv_obj) },
	{ MP_ROM_QSTR(MP_QSTR_pixel), MP_ROM_PTR(&display_pixel_obj) },
	{ MP_ROM_QSTR(MP_QSTR_backlight), MP_ROM_PTR(&display_backlight_obj) },
	{ MP_ROM_QSTR(MP_QSTR_line), MP_ROM_PTR(&display_line_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rect), MP_ROM_PTR(&display_rect_obj) },
	{ MP_ROM_QSTR(MP_QSTR_circ), MP_ROM_PTR(&display_circ_obj) },
	{ MP_ROM_QSTR(MP_QSTR_clear), MP_ROM_PTR(&display_clear_obj) },
	{ MP_ROM_QSTR(MP_QSTR_update), MP_ROM_PTR(&display_update_obj) },

#if 1
	{ MP_ROM_QSTR(MP_QSTR_line_to), MP_ROM_PTR(&display_line_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_move_to), MP_ROM_PTR(&display_move_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_curve_to), MP_ROM_PTR(&display_curve_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_arc), MP_ROM_PTR(&display_arc_obj) },
	{ MP_ROM_QSTR(MP_QSTR_quad_to), MP_ROM_PTR(&display_quad_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_linear_gradient), MP_ROM_PTR(&display_linear_gradient_obj) },
	{ MP_ROM_QSTR(MP_QSTR_radial_gradient), MP_ROM_PTR(&display_radial_gradient_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rectangle), MP_ROM_PTR(&display_rectangle_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rel_line_to), MP_ROM_PTR(&display_rel_line_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rel_move_to), MP_ROM_PTR(&display_rel_move_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rel_curve_to), MP_ROM_PTR(&display_rel_curve_to_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rel_quad_to), MP_ROM_PTR(&display_rel_quad_to_obj) },

	{ MP_ROM_QSTR(MP_QSTR_text), MP_ROM_PTR(&display_text_obj) },
	{ MP_ROM_QSTR(MP_QSTR_font), MP_ROM_PTR(&display_font_obj) },
	{ MP_ROM_QSTR(MP_QSTR_scale), MP_ROM_PTR(&display_scale_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rotate), MP_ROM_PTR(&display_rotate_obj) },
	{ MP_ROM_QSTR(MP_QSTR_font_size), MP_ROM_PTR(&display_font_size_obj) },
	{ MP_ROM_QSTR(MP_QSTR_line_width), MP_ROM_PTR(&display_line_width_obj) },
	{ MP_ROM_QSTR(MP_QSTR_gray), MP_ROM_PTR(&display_gray_obj) },
	{ MP_ROM_QSTR(MP_QSTR_global_alpha), MP_ROM_PTR(&display_global_alpha_obj) },
	{ MP_ROM_QSTR(MP_QSTR_translate), MP_ROM_PTR(&display_translate_obj) },
	{ MP_ROM_QSTR(MP_QSTR_rgba), MP_ROM_PTR(&display_rgba_obj) },
	{ MP_ROM_QSTR(MP_QSTR_fill), MP_ROM_PTR(&display_fill_obj) },
	{ MP_ROM_QSTR(MP_QSTR_clip), MP_ROM_PTR(&display_clip_obj) },
	{ MP_ROM_QSTR(MP_QSTR_gradient_add_stop), MP_ROM_PTR(&display_gradient_add_stop_obj) },
	{ MP_ROM_QSTR(MP_QSTR_stroke), MP_ROM_PTR(&display_stroke_obj) },
	{ MP_ROM_QSTR(MP_QSTR_save), MP_ROM_PTR(&display_save_obj) },
	{ MP_ROM_QSTR(MP_QSTR_restore), MP_ROM_PTR(&display_restore_obj) },
	{ MP_ROM_QSTR(MP_QSTR_begin_path), MP_ROM_PTR(&display_begin_path_obj) },
	{ MP_ROM_QSTR(MP_QSTR_close_path), MP_ROM_PTR(&display_close_path_obj) },
	//{ MP_ROM_QSTR(MP_QSTR_flush), MP_ROM_PTR(&display_flush_obj) },
#endif
};
static MP_DEFINE_CONST_DICT(
	display_module_globals, display_module_globals_table
);

const mp_obj_module_t display_module = {
	.base    = { &mp_type_module },
	.globals = (mp_obj_dict_t *)&display_module_globals,
};

/* clang-format off */
MP_REGISTER_MODULE(MP_QSTR_sys_display, display_module, MODULE_DISPLAY_ENABLED);
